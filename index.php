<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *	Still rocking comments all the time. 
 * 	This will be the final comment for now.
 * @package WordPress
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *	Rockin'the comments section again :D
 * @var bool
 */
define('WP_USE_THEMES', true);

/** Loads the WordPress Environment and Template */
require( dirname( __FILE__ ) . '/wp-blog-header.php' );
